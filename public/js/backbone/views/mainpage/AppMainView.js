var app = app || {};

$(function( $ ) {
	'use strict';

	app.AppMainView = Backbone.View.extend({
		
		el: '#app-container',

		events: {
			
		},

		initialize: function() {
 			this.headerMenuView = new app.HeaderMenuView();
			this.faakListView = new app.FaakListView({collection: app._Faaks});
			this.render();
		},

		render: function() {
			//render only header
			this.headerMenuView.render();
		},

	});
});
