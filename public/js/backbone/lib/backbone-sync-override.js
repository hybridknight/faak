Backbone.sync = function (method, model, options) {
    var socket = window.socket; // grab active socket from global namespace; io.connect() was used to create socket
    // console.log(!!model.collection);
    var isModel = !!model.collection;
    // Save a new model to the server.
    var create = function () {  
        if(isModel){
            socket.emit('model:create', model.attributes);
        }else{

        }
    };              
 
    // Get a collection or model from the server.
    var read = function () {
        if(isModel){
            socket.emit('model:read');
        }else{
            socket.emit('collection:read'); 
        }
    }; 
     
    // Save an existing model to the server.
    var update = function () {
        var sign = signature(model); 
        var e = event('update', sign);
        socket.emit('update', {'signature' : sign, item : model.attributes }); // model.attribues is the model data
        socket.once(e, function (data) { 
            options.success();                   
        });                           
    };  
     
    // Delete a model on the server.
    var destroy = function () {
        var sign = signature(model); 
        var e = event('delete', sign);
        socket.emit('delete', {'signature' : sign, item : model.attributes }); // model.attribues is the model data
        socket.once(e, function (data) { 
            console.log(data);                     
        });                           
    };             
       
    // entry point for method
    switch (method) {
        case 'create':
            create();
            break;        
        case 'read':  
            read(); 
            break;  
        case 'update':
            //update();
            break;
        case 'delete':
            destroy();
            break; 
    }        
};