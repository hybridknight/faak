var app = app || {};

(function() {
	'use strict';

	// Todo Collection
	// ---------------

	// The collection of todos is backed by *localStorage* instead of a remote
	// server.
	app.Things = Backbone.Collection.extend({
		url: 'things',
		model: app.Thing,
		socket: window.socket,
		initialize: function(parentModel){
			this.parentModel = parentModel;
			_.bindAll(this, 'serverCreate', 'serverDelete', 'collectionCleanup');
			this.ioBind('create', this.serverCreate, this);
			//this.ioBind('delete', this.serverDelete, this);
		},
		serverCreate: function(data){
			var exists = this.get(data._id);
			if( ! exists){
				this.add(data);
			}else{
				data.fromServer = true;
				exists.set(data);
			}
		},
		serverDelete: function(data){

		},
		collectionCleanup: function(callback){
			this.ioUnBindAll();
			this.each(function(model){
				model.modelCleanup();
			});
			return this;
		}
	});
	
}());
