var app = app || {};
var ENTER_KEY = 13;
var socket = io.connect();
var username;
socket.on('connect', function(){
  socket.once('username', function(username){
    window.username = username;
    $(document).ready(function(){
		app._Faaks = app._Faaks || new app.Faaks();
		app._AppMainView = app._AppMainView || new app.AppMainView();

		app._Faaks.fetch();
	});
  });
});

